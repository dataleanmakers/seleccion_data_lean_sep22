## Prueba seleccion Data Lean Makers

1.- Crear una aplicación web con un login simple, un sidebar con un menú desplegable, un navbar con un logout. El menú desplegable del sidebar debe tener solo dos enlaces que muestre una tabla con buscador, selección de filas por página y paginador, y el segundo muestra unas tarjetas con datos. Los datos hay que consumirlos desde una API utilizando la página https://jsonplaceholder.typicode.com/ donde se pueden conseguir datos utilizando los verbos para peticiones HTTP. Se deben utilizar  GET, POST, PUT, DELETE en la tabla, y GET en la vista de tarjetas.  
Se pueden utilizar librerías importadas con NPM.  


2.- Las tecnologías Front a utilizar deben ser componentes web, bien sea Web-Components nativos o Lit.  

3.- Se valorará especialmente: 

 3.1.-serializar datos en uno o varios ficheros JSON, y utilizar estos datos en la aplicación.  

3.2.- Crear una base de datos en SQlite, MySQL, MongoDB, u otras y utilizar en la aplicación, no sustituyendo la base de datos a los datos traídos de la API de la página mencionada  

3.3.- Crear un Readme.md con las especificaciones del sistema creado. Este fichero de especificaciones debe estar escrito en MarkDown  


El repositorio que hay que clonar es el siguiente https://gitlab.com/dataleanmakers/seleccion_data_lean_sep22.git  

El sistema creado debe subirse al repositorio en la rama (nombre y apellidos del candidato sin espacios y en camelCase)  

Tiempo de ejecución de la prueba 2 horas y 30 minutos   
